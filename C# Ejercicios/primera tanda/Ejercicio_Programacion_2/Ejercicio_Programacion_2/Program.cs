﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_Programacion_2
{
    class Program
    {
        static void Main(string[] args)
        {


            // Escribe un script que muestre por consola todos los números comprendidos entre el 1 y el 100 que no
            //sean múltiplos de 3 o 5

            for (int cont = 1; cont <= 100; cont++)
            {
                if (cont % 3 != 0 && cont % 5 != 0)
                {
                    Console.WriteLine(cont);
                }
            }
            Console.ReadKey();
            Console.WriteLine("Codigo escrito por Marcos Somoza.");
            Console.ReadKey();
        }
    }
}