﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_Programacion_4
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("Dime un año y te digo si es bisiesto o no:");

            string año = Console.ReadLine();
            int anio = int.Parse(año);

            if ((anio % 4 == 0 && anio % 100 != 0) || anio % 400 == 0)
            {
                Console.Write(anio);
                Console.WriteLine(" es bisiesto");
            }

            else
            {
                Console.Write(anio);
                Console.WriteLine(" no es bisiesto");
            }


            Console.ReadKey();


        }
    }
}