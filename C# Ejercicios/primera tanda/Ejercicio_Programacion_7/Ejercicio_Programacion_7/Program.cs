﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_programacion_7
{
    class Program
    {

        static int[] Invertir(int[] datos)
        {

            int longitud = datos.Length;
            int[] inverso = datos;
            for (int i = 0; i < longitud / 2; i++)
            {
                int aux = datos[i];
                datos[i] = inverso[(longitud - 1) - i];
                inverso[(longitud - 1) - i] = aux;

            }


            return inverso;
        }


        static void Main(string[] args)
        {




            int[] datos = { 1, 2, 3, 4 };
            int longitud = datos.Length;


            Console.Write("array original:");
            for (int i = 0; i < longitud; i++)
            {
                Console.Write("{0}", datos[i]);
            }



            int[] araisito = Invertir(datos);



            Console.WriteLine(" ");

            Console.Write("array invertido:");
            for (int i = 0; i < longitud; i++)
            {
                Console.Write("{0}", araisito[i]);
            }



            Console.ReadKey();


        }
    }
}