﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_Programacion_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Sumando Pares: ");
            int j = 0;
            for (int i = 2; i <= 499; i++)
            {
                if (i % 2 == 0)
                {
                    j += i;
                    Console.WriteLine(j);
                }
            }
            Console.ReadKey();

            Console.WriteLine("Sumando Impares: ");

            int k = 0;
            for (int l = 2; l <= 499; l++)
            {
                if (l % 2 != 0)
                {
                    k += l;
                    Console.WriteLine(k);
                }

            }

            Console.ReadKey();

            Console.WriteLine("Codigo escrito por Marcos Somoza.");

            Console.ReadKey();
        }
    }
}