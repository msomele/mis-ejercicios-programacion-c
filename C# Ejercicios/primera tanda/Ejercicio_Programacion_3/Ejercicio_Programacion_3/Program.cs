﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_Programacion_3
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("¿Que nota has sacado?");
            Console.WriteLine("del uno al diez");
            Console.WriteLine("(los decimanes con comas \",\") ");

            string dato = Console.ReadLine();
            float nota = float.Parse(dato);

            if (nota >= 9)
            {
                Console.WriteLine("Sobresaliente");
            }

            else if (nota >= 7)
            {
                Console.WriteLine("Notable");
            }

            else if (nota >= 6)
            {
                Console.WriteLine("Bien");
            }

            else if (nota >= 5)
            {
                Console.WriteLine("Suficiente");
            }

            else
            {
                Console.WriteLine("Suspenso");
            }

            Console.ReadKey();
        }
    }
}