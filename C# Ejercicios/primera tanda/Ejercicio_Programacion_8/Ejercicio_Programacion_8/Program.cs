﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_Programacion_8
{
    class Program
    {



       static public int basicArrayCalculator(int[] miArray, char operacion)
        {

            int longitud = miArray.Length;
            int result = 0;

            switch (operacion)
            {
                case '+':
                        for(int i = 0; i < longitud; i++)
                    {
                        result += miArray[i];
                    }
                    break;
                case '-':
                    for (int i = 0; i < longitud; i++)
                    {
                        result -= miArray[i];
                    }
                    break;
                case '*':
                    for (int i = 0; i < longitud; i++)
                    {
                        result *= miArray[i];
                    }
                    break;
            }
            return result;
        }


        static public int[] getRandomArray(int longitud, int min, int max)
        {

            int[] ArrayNormal = new int[longitud];

            Random aleatorio = new Random();

            for (int i = 0; i < longitud; i++)
            {
                ArrayNormal[i] = aleatorio.Next(min, max);
            }
            return ArrayNormal;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Dime la longitud del array: ");
            int l= int.Parse(Console.ReadLine());
            Console.WriteLine("Dime el num min del array: ");
            int min = int.Parse(Console.ReadLine());
            Console.WriteLine("Dime el num max del array: ");
            int max = int.Parse(Console.ReadLine());
            int[] Elprinteo = getRandomArray(l,min,max);

            Console.WriteLine("Dime el signo de operacion(+ - *): ");
            char sig = char.Parse(Console.ReadLine());

            for(int t = 0; t < l; t++)
            {
                Console.WriteLine(Elprinteo[t]);
            }

            Console.WriteLine("El resultado es: {0}",basicArrayCalculator(Elprinteo, sig));
            Console.ReadKey();
        }
    }
}
