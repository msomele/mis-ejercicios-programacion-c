﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    protected static int Fila;
    protected static int Colum;

    protected static void WriteAt(string texto, int x, int y)
    {

        Console.SetCursorPosition(Colum + x, Fila + y);
        Console.Write(texto);


    }

    public static void Main()
    {
        /*Crea una función que pida un string al usuario y la posición en la pantalla a partir de la cual se
        empezará a imprimir el string.*/
        Console.Clear();
        Fila = Console.CursorTop;
        Colum = Console.CursorLeft;
        Console.WriteLine("dame el texto: ");
        string texto = Convert.ToString(Console.ReadLine());
        Console.WriteLine("dame la posicion x: ");
        int x = Convert.ToInt16(Console.ReadLine());
        Console.WriteLine("dame la posicion y: ");
        int y = Convert.ToInt16(Console.ReadLine());
        Console.Clear();
        WriteAt(texto, x, y);

        Console.WriteLine();
        Console.ReadLine();

    }


}
