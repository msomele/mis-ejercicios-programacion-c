﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kk
{
    class Program
    {





        static void Main(string[] args)
        {


            /*5. Crea un programa que solicite un string al usuario y realice las operaciones necesarias para mostrar
            los siguientes resultados de salida por consola.
            ▪ Sustituir todos los espacios por guiones bajos.
            ▪ Hacer que todas las vocales aparezcan en mayúsculas.
            ▪ Trasformar la cadena de texto a formato ascii numérico.*/



            Console.WriteLine("Dame un texto a transformar: ");
            string texto = Convert.ToString(Console.ReadLine()); //tomo el string


            var textito = texto.ToCharArray(); //paso el string a string[] para poder escribir sobre el ya que los string son solo de tipo lectura
            int txtlen = textito.Length;

            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("con '_' en lugar de ' ':");
            Console.WriteLine(" ");

            for (int i = 0; i < txtlen; i++)
            {
                if (textito[i] == ' ')
                {
                    textito[i] = '_';
                }
                Console.Write(textito[i]); //cambio los caracteres que sean espacios e imprimo
            }
            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("añadimos que las vocales en minus sean manus:");
            Console.WriteLine(" ");

            for (int j = 0; j < txtlen; j++)
            {
                if (textito[j] == 'a' || textito[j] == 'e' || textito[j] == 'i' || textito[j] == 'o' || textito[j] == 'u')
                {
                    int aux = (int)textito[j] - 32;
                    char aux2 = (char)aux;
                    textito[j] = aux2;
                }
                Console.Write(textito[j]); //eligo que solo ocurra si se trata de vocales, luego las paso a mayus restando 32 en ascii y las vuelvo a poner en char
            }
            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("por ultimo pasamos todo a ascii:");
            Console.WriteLine(" ");

            for (int k = 0; k < textito.Length; k++)
            {
                int ASCII = (int)textito[k];
                Console.Write("{0} ", ASCII); //usando el metodo anterior paso a ascii todos los caracteres y los imprimo
            }






            Console.ReadKey();



        }
    }
}