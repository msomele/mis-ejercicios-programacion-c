﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_Programacion_17
{
    class Program
    {
       
        static public string palin (string dato)
        {
            string Pali = "es palindromo";
            string NoPali = "no es palindromo";
            var palin = dato.ToCharArray(); //basicamente aqui paso el string a un array para poder trabajar con cada caracter en una posicion del mismo
            int palinlen = palin.Length;
            int contador = 0;
            for (int i = 0; i < palinlen / 2; i++)
            {
                if (palin[i] == palin[palinlen - i - 1])  //simplemente una forma de comprobar que el caracter de la primera posicion sea igual al de la ultima
                {
                    contador++; //este contador es una forma de comprobar que se han mirado todos los caracteres del array
                }
            }

            if (contador == palinlen / 2)   //si el contador vale igual que la len/2 esta claro que es palindromo pero sino no
                return Pali;
            else return NoPali;
        }




        static void Main(string[] args)
        {
            Console.WriteLine("Dame una palabra a analizar: ");
            string dato = Convert.ToString(Console.ReadLine());
            Console.WriteLine(palin(dato));

            Console.ReadKey();
        }
    }
}
