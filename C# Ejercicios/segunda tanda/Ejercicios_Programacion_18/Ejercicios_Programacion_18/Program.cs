﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_Programacion_18
{

    static public class Encriptacion
    {
        static public string Encri(string datos)
        {

            char [] datosarr = datos.ToCharArray();
            int datoslen = datosarr.Length;
            for (int j = 0; j < datoslen; j++)
            {
                if (datosarr[j] != ' ')
                {
                    int aux = (int)datosarr[j] - 1;
                    char aux2 = (char)aux;
                    datosarr[j] = aux2;
                }



            }
            string devolver = new string(datosarr);
            return devolver;
        }



        static public string Desencri(string datos)
        {
            char[] datosarr = datos.ToCharArray();
            int datoslen = datosarr.Length;
            for (int j = 0; j < datoslen; j++)
            {
                if (datosarr[j] != ' ')
                {
                    int aux = (int)datosarr[j] + 1;
                    char aux2 = (char)aux;
                    datosarr[j] = aux2;
                }



            }
            string devolver = new string(datosarr);
            return devolver;
        }



    }



    class Program
    {
        static void Main(string[] args)
        {
            /*Desarrollar una clase que permita encriptar y desencriptar texto. Para ello crearemos una clase
            estática, en la que se incluirán dos métodos también estáticos, uno para encriptar y otro para
            desencriptar.*/

            Console.WriteLine("deseas encriptar o desencriptar (1/2): ");
            int eleccion = int.Parse(Console.ReadLine());
            Console.WriteLine("dame lo que desee encriptar:");
            string datos = Console.ReadLine();
            if(eleccion == 1)
            {
                Console.WriteLine(Encriptacion.Encri(datos));
            }
            if (eleccion == 2
                )
            {
                Console.WriteLine(Encriptacion.Desencri(datos));
            }

           
            Console.ReadKey();



        }
    }
}
