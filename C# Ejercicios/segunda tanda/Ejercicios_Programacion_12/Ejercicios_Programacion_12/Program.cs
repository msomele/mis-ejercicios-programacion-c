﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_Programacion_12
{
    class Program
    {

        static int [] MaxMinArray(ref int refmin, ref int refmax, int [] array)
        {
            refmax = array[0];
            refmin = array[0];

            for (int i = 0; i < array.Length; i++)
            {


                if (refmax <= array[i])
                {
                    refmax = array[i];
                }
                if (refmin >= array[i])
                {
                    refmin = array[i];
                }
            }

            return array;

        }




        static void Main(string[] args)
        {

            /* Desarrolla una función llamada minMaxArray, que devuelva el valor mínimo y máximo almacenados
            en un array. Para ello deberá hacerse uso de parámetros por referencia. La llamada al método debe ser
            minMaxArray(array, ref min, ref max ). Después de la ejecución las variables min y max deben mantener
            su valor*/



            int[] array = {5,-123,699,345,-500,1000};

            int min = 0;
            int max = 0;

            MaxMinArray(ref min,ref max, array);
            

            for(int i = 0; i < array.Length ; i++)
            {
                Console.Write(" {0} ", array[i]);
            }

            Console.WriteLine(" ");

            Console.WriteLine("el valor minimo es: {0}", min);
            Console.WriteLine("el valor maximo es: {0}", max);
            Console.ReadKey();







        }
    }
}
