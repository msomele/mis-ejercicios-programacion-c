﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_Programacion_16
{
    class Program
    {

        static public int factorial (int num)
        {
            int[] almacen = new int[num];
            for (int i = 0; i < num; i++)
            {
                almacen[i] = num - i;
                
            }
            int sol = 1;
            for (int j = 0; j < num - 1; j++)
            {
                sol *= almacen[j];
            }
            return sol;
        }




        static void Main(string[] args)
        {

            // Desarrolla una función que permita calcular el factorial de un número dado por consola.
            Console.WriteLine("dame el numero al que se desee sacar su factorial: ");
            int dato = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("su factorial es: {0}",factorial(dato));

            Console.ReadLine();

        }
    }
}
