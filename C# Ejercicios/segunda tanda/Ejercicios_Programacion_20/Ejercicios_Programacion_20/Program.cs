﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_Programacion_20
{
    class Program
    {
        static public int potencia(int numero, int elevado)
        {
            int sol = numero;
            for(int i = 0; i < elevado; i++)
            {
                sol *= numero;
                Console.WriteLine(sol);
            }
            
            return sol;
        }


        static void Main(string[] args)
        {
            /*Crea una función para calcular la potencia de un número dado, elevado a otro número dado. La
             función deberá devolver el resultado de la operación. Por ejemplo potencia(2, 3) deberá devolver 8. No
             está permitido el uso de la biblioteca Math.*/


            Console.WriteLine("Dame el numero : ");
            int numero = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Dame la potencia : ");
            int elevado = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(potencia(numero, elevado));
            Console.ReadKey();

        }
    }
}
